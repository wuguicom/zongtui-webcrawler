package com.zongtui.fourinone.worker;

import com.zongtui.fourinone.delegate.Delegate;
import com.zongtui.fourinone.delegate.DelegatePolicy;
import com.zongtui.fourinone.exception.RecallException;
import com.zongtui.fourinone.file.WareHouse;
import com.zongtui.fourinone.park.ParkObjValue;
import com.zongtui.fourinone.park.ParkPatternBean;
import com.zongtui.fourinone.park.ParkPatternExector;

final public class WorkerParkProxy
{
	private String domainnodekey;
	private RecallException rx;
	
	public WorkerParkProxy(String domainnodekey)
	{
		this.domainnodekey = domainnodekey;
		rx = new RecallException();
	}
	
	@Delegate(interfaceName="com.zongtui.fourinone.WorkerLocal",methodName="doTask",policy= DelegatePolicy.Implements)
	public WareHouse doTaskParkProxy(WareHouse inhouse){
		if(rx.tryRecall(inhouse)==-1)
			return null;
		
		//System.out.println("doTaskParkProxy:"+inhouse);
		WareHouse outhouse = new WareHouse(false);
		//domain,node,inhouse,outhouse->ParkPatternBean->ParkPatternExector to park and get whLastest
		String[] keyarr = ParkObjValue.getDomainNode(domainnodekey);
		ParkPatternBean ppb = new ParkPatternBean(keyarr[0],keyarr[1],inhouse,outhouse,rx);
		ParkPatternExector.append(ppb);
		return outhouse;
	}
}