package com.zongtui.fourinone.obj;

import com.zongtui.fourinone.park.ParkObjValue;

public class ObjectBeanProxy implements ObjectBean{
	public Object obj;
	public Long vid;
	public String name;
	public ObjectBeanProxy(){}
	/*private ObjectBeanProxy(ObjValue ov, String domainnodekey){
		vid = (Long)ov.getObj(domainnodekey+"._me_ta.version");
		obj = ov.get(domainnodekey);
		name = domainnodekey;
	}*/
	//@Delegate(interfaceNacom.zongtui.fourinone.obj.ObjectBeanBean",methodName="toObject",policy=DelegatePolicy.Implements)
	public Object toObject(){
		return obj;
	}
	//@Delegate(interfaceNacom.zongtui.fourinone.obj.ObjectBeanBean",methodName="getName",policy=DelegatePolicy.Implements)
	public String getName(){
		return name;
	}
	
	public String getDomain(){
		if(name!=null)
			return ParkObjValue.getDomainNode(name)[0];
		else
			return null;
	}	
		
	public String getNode(){
		if(name!=null)
		{
			String[] arr = ParkObjValue.getDomainNode(name);
			if(arr.length==2)
				return arr[1];
		}
		return null;
	}
	
	
	public String toString(){
		return name+":"+obj.toString();
	}
	/*@Delegate(interfaceNacom.zongtui.fourinone.obj.ObjectVersionsion",methodName="getVid",policy=DelegatePolicy.Implements)
	public Long getVid(){
		return vid;
	}*/
}