package com.zongtui.fourinone.base.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * 资源对象.
 */
public class MulBean extends ResourceBean {
    //本地的编码.
    private String nativeLangCode;

    /**
     * 构造函数.
     *
     * @param langCode 编码
     */
    public MulBean(String langCode) {
        super();
        //默认路径是config
        resourcesName = "config";
        //初始化.
        init(langCode);
    }

    /**
     * 初始化方法.
     *
     * @param langCode 编码
     */
    public void init(String langCode) {
        if (langCode == null) {
            bundle = ResourceBundle.getBundle(resourcesName, Locale.getDefault());
        } else if (langCode.toUpperCase().equals("ISO-8859-1")) {
            nativeLangCode = "ISO-8859-1";
            bundle = ResourceBundle.getBundle(resourcesName, Locale.US);
        } else if (langCode.toUpperCase().equals("GB2312")) {
            nativeLangCode = "GB2312";
            bundle = ResourceBundle.getBundle(resourcesName, Locale.PRC);
        } else if (langCode.toUpperCase().equals("BIG5")) {
            nativeLangCode = "BIG5";
            bundle = ResourceBundle.getBundle(resourcesName, Locale.TAIWAN);
        }
    }

    /**
     * 获得属性
     *
     * @param keyWord 属性名称.
     * @return 获得属性
     */
    public String getString(String keyWord) {
        return getString(keyWord, "");
    }

    /**
     * 获得属性
     *
     * @param keyWord 属性名
     * @param topStr  未找到的默认值
     * @return 获得属性
     */
    public String getString(String keyWord, String topStr) {
        String str = "";
        try {
            str = bundle.getString(keyWord);

        } catch (MissingResourceException ex) {
            str = topStr + keyWord;
        }
        return str;
    }

    /**
     * 获得空格字段.
     *
     * @return 获得空格字段.
     */
    public String getSpace() {
        String space = "";
        if (nativeLangCode != null && nativeLangCode.equals("ISO-8859-1"))
            space = "&nbsp;";

        return space;
    }

    /**
     * 获得文件数据.
     *
     * @param relativeUri 相对路径.
     * @return 返回文件数据
     */
    public String getFileString(String relativeUri) {
        StringBuffer sb = new StringBuffer();
        try {
            Reader f = new InputStreamReader(this.getClass().getResourceAsStream(relativeUri));
            BufferedReader fb = new BufferedReader(f);
            String s;
            while ((s = fb.readLine()) != null) {
                sb = sb.append(s);
            }
            f.close();
            fb.close();
        } catch (IOException ex) {
            //noinspection ThrowablePrintedToSystemOut
            System.err.println(ex);
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        MulBean rb = new MulBean("GB2312");
        //try{Thread.sleep(10000L);}catch(Exception ex){}
        System.out.println(rb.getString("QSXYSJ"));
        System.out.println(rb.getString("YBB"));
        System.out.println(rb.getString("YGSJ"));
    }
}
